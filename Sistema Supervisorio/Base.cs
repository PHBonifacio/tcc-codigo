﻿#define TEST

using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Threading;
using System.Timers;
using System.IO.Ports;
using System.Net;
using System.Net.Sockets;
using System.IO;
using LiveCharts.Configurations;
using LiveCharts; //Core of the library
using LiveCharts.Wpf; //The WPF controls
using LiveCharts.WinForms; //the WinForm wrappers
using System.Collections;
using OfficeOpenXml;
using Newtonsoft.Json;

namespace Sistema_Supervisorio
{
    public partial class ControlSystem : Form
    {
        #region Variables

        class ClientContext
        {
            public TcpClient Client;
            public Stream Stream;
            public byte[] Buffer = new byte[4];
            public MemoryStream Message = new MemoryStream();
        }

        class ParametrosSenders
        {
            public ParametrosSenders(byte id)
            {
                Address = id;
                for (int i = 0; i < 256; i++)
                {
                    Lista_Acelerometro.Add(new byte[6]);
                    Lista_STemp.Add(new byte[6]);
                }
            }
            IEnumerable<byte[]> capacity;
            public byte Address;
            public List<byte[]> Lista_Acelerometro = new List<byte[]>(256);
            public List<byte[]> Lista_STemp = new List<byte[]>(256);
        }

        public class Sensor
        {
            public string Tipo;
            public string Data;
            public string Hora;
            public string ID_TX;
            public string ID_sensor;
            public List<string> Dados;
            public Sensor()
            {
                Dados = new List<string>();
                Data = "";
                Hora = "";
            }
        }

        List<ParametrosSenders> Lista_Senders = new List<ParametrosSenders>(256);
        System.Timers.Timer general_timer = new System.Timers.Timer(5000);
        SerialPort serialport = new SerialPort();
        static TcpListener servidorTCP = null;
        NetworkStream stream_data;
        byte[] s = { 0x01, 0x02 };
        List<byte> ListaID_Temperatura = new List<byte>(256);
        List<byte> ListaID_Corrente = new List<byte>(256);
        List<byte> ListaID_TX = new List<byte>(256);
        List<Sensor> ListaSensorJSON = new List<Sensor>(1000);
        List<byte[]> ListaHandleMSG = new List<byte[]>(65535);
        byte flag_status_servidorTCP = 0x00;
        Protocolo Mensagem = new Protocolo();
        private BackgroundWorker bw_message;
        static private BackgroundWorker bw_message_TCP1;
        static private BackgroundWorker bw_message_TCP2;
        static private BackgroundWorker bw_message_TCP3;
        private BackgroundWorker bw_table;
        private BackgroundWorker bw_Add_JSON;
        FileInfo TabelaExcel;
        String FolderPath = Path.Combine(Path.GetDirectoryName(System.Reflection.Assembly.GetExecutingAssembly().Location));
        string[] FilesOnFolder;
        #endregion

        public ControlSystem()
        {
            InitializeComponent();
            WindowState = FormWindowState.Maximized; // TODO
            general_timer.Elapsed += General_timer_Handler;
            this.bw_message = new BackgroundWorker();
            this.bw_message.DoWork += new DoWorkEventHandler(bw_message_DoWork);
            bw_message.RunWorkerAsync();

            bw_Add_JSON = new BackgroundWorker();
            bw_Add_JSON.DoWork += new DoWorkEventHandler(bw_Add_JSON_DoWork);
            bw_Add_JSON.RunWorkerAsync();
            bw_message_TCP1 = new BackgroundWorker();
            bw_message_TCP1.DoWork += new DoWorkEventHandler(bw_message_DoWork);

            bw_message_TCP2 = new BackgroundWorker();
            bw_message_TCP2.DoWork += new DoWorkEventHandler(bw_message_DoWork);

            bw_message_TCP3 = new BackgroundWorker();
            bw_message_TCP3.DoWork += new DoWorkEventHandler(bw_message_DoWork);


            this.bw_table = new BackgroundWorker();
            this.bw_table.DoWork += new DoWorkEventHandler(bw_table_DoWork);
        }

        private void bw_Add_JSON_DoWork(object sender, DoWorkEventArgs e)
        {
            if (ListaSensorJSON.Count > 0)
            {
                Sensor dados_recebidos = ListaSensorJSON[0];
                if (dados_recebidos != null)
                {
                    dados_recebidos.Data = string.Format("{0}/{1}/{2}", DateTime.Now.ToString("dd"), DateTime.Now.ToString("MM"), DateTime.Now.ToString("yy"));
                    dados_recebidos.Hora = string.Format("{0}:{1}:{2}", DateTime.Now.ToString("HH"), DateTime.Now.ToString("mm"), DateTime.Now.ToString("ss"));

                    string json_string = JsonConvert.SerializeObject(dados_recebidos);



                    FilesOnFolder = Directory.GetFiles(FolderPath);
                    bool teste = FilesOnFolder.Contains(FolderPath + string.Format("\\Json ID {0}.txt", dados_recebidos.ID_TX));
                    if (!FilesOnFolder.Contains(FolderPath + string.Format("\\Json ID {0}.txt", dados_recebidos.ID_TX)))
                    {
                        // Create a file to write to.
                        using (StreamWriter sw = File.CreateText(FolderPath + string.Format("\\Json ID {0}.txt", dados_recebidos.ID_TX)))
                        {
                            sw.Write("[" + json_string);
                            sw.Close();
                            sw.Dispose();

                        }
                    }
                    else
                    {
                        using (StreamWriter sw = File.AppendText(FolderPath + string.Format("\\Json ID {0}.txt", dados_recebidos.ID_TX)))
                        {
                            sw.Write(",\r\n" + json_string);
                            sw.Close();
                            sw.Dispose();
                        }
                    }
                    ListaSensorJSON.RemoveAt(0);
                }
            }
        }


        private void bw_table_DoWork(object sender, DoWorkEventArgs e)
        {
            BackgroundWorker worker = (BackgroundWorker)sender;
            try
            {
                byte[] recebido = (byte[])e.Argument;
                Handle_Table(recebido);
            }
            catch (Exception ex)
            {
                MessageBox.Show(e.ToString());
            }

        }

        private void ControlSystem_Load(object sender, EventArgs e)
        {
            Find_IP();
            Update_COM();
            general_timer.AutoReset = true;
            general_timer.Start();

            Random r = new Random();
            System.Windows.Media.Color myColor = System.Windows.Media.Color.FromArgb((byte)r.Next(0, 256), (byte)r.Next(0, 256), (byte)r.Next(0, 256), (byte)r.Next(0, 256));
            System.Windows.Media.SolidColorBrush myBrush = new System.Windows.Media.SolidColorBrush(myColor);
            
            cartesian_STemp.Series.Add(new LineSeries
            {
                Title = "Aceleração",
                LineSmoothness = 1, //straight lines, 1 really smooth lines
                                    //PointGeometry = System.Windows.Media.Geometry.Parse("m 25 70.36218 20 -28 -20 22 -8 -6 z"),
                PointGeometrySize = 5,
                PointForeground = myBrush
            });
            cartesian_STemp.Series[0].Values = (new ChartValues<double>());

            myColor = System.Windows.Media.Color.FromArgb((byte)r.Next(0, 256), (byte)r.Next(0, 256), (byte)r.Next(0, 256), (byte)r.Next(0, 256));
            myBrush = new System.Windows.Media.SolidColorBrush(myColor);

            cartesian_STemp2.Series.Add(new LineSeries
            {
                Title = "Temperatura do Objeto",
                LineSmoothness = 1, //straight lines, 1 really smooth lines
                                    //PointGeometry = System.Windows.Media.Geometry.Parse("m 25 70.36218 20 -28 -20 22 -8 -6 z"),
                PointGeometrySize = 5,
                PointForeground = myBrush
            });
            cartesian_STemp2.Series[0].Values = (new ChartValues<double>());

            myColor = System.Windows.Media.Color.FromArgb((byte)r.Next(0, 256), (byte)r.Next(0, 256), (byte)r.Next(0, 256), (byte)r.Next(0, 256));
            myBrush = new System.Windows.Media.SolidColorBrush(myColor);

            cartesian_STemp3.Series.Add(new LineSeries
            {
                Title = "Temperatura do Ambiente",
                LineSmoothness = 1, //straight lines, 1 really smooth lines
                                    //PointGeometry = System.Windows.Media.Geometry.Parse("m 25 70.36218 20 -28 -20 22 -8 -6 z"),
                PointGeometrySize = 5,
                PointForeground = myBrush
            });
            cartesian_STemp3.Series[0].Values = (new ChartValues<double>());
        }

        private void CartesianChart1OnDataClick(object sender, ChartPoint chartPoint)
        {
            MessageBox.Show("You clicked (" + chartPoint.X + "," + chartPoint.Y + ")");
        }
        
        #region BackgroundWorkers

        private void bw_message_DoWork(object sender, DoWorkEventArgs e)
        {
            BackgroundWorker worker = (BackgroundWorker)sender;
            try
            {
                byte[] msg;
                while (ListaHandleMSG.Count > 0)
                {
                    msg = new byte[ListaHandleMSG[0].Length];
                    msg = ListaHandleMSG[0];
                    if (msg != null)
                    {
                        Handle_Big_Frame(msg);
                        ListaHandleMSG.RemoveAt(0);
                    }

                }
            }

            catch (Exception ex)
            {

            }

        }

        public void Handle_Big_Frame(byte[] vetor)
        {
            int i, j, k;
            byte[] aux;
            for (i = 0; i < vetor.Length; i++)
            {
                if (vetor[i] == 0x01)
                {
                    aux = new byte[8];
                    Array.Copy(vetor, i, aux, 0, aux.Length);
                    Handle_Frame(aux);

                }
            }
        }

        #endregion
        #region Delegates functions
        /// <summary>
        /// Função Delegate genérica para funções void sem parâmetro
        /// </summary>
        public delegate void Delegate_void_NoPam();
        public delegate void Delegate_void_String(string color, string s);
        public delegate void Delegate_void_Serial_bytearray(byte[] msg);
        public delegate void Delegate_void_Handle_Array(Int16 array);
        public delegate void Delegate_Change_RichTextBox(RichTextBox textbox, Color cor, string texto);
        public delegate void Delegate_Button_Perform_Click(Button botao);
        #endregion
        public void Button_Perform_Click(Button botao)
        {
            botao.PerformClick();
        }
        private void Escreve_textLOG(string color, string str)
        {
            string data = string.Format("[{0}/{1}/{2} - {3}:{4}:{5}]", DateTime.Now.ToString("dd"), DateTime.Now.ToString("MM"), DateTime.Now.ToString("yy"), DateTime.Now.ToString("HH"), DateTime.Now.ToString("mm"), DateTime.Now.ToString("ss"));
            string string_aux;
            try
            {
                if (str == null) { return; }
                string_aux = string.Format("{0} : {1} \r\n\n", data, str);
                textLOG.AppendText(string_aux);
                textLOG.Select((textLOG.Text.Length - string_aux.Length), string_aux.Length);
                switch (color)
                {
                    case "vermelho":
                        textLOG.SelectionColor = Color.Red;
                        break;
                    case "azul":
                        textLOG.SelectionColor = Color.Blue;
                        break;
                    default:
                        textLOG.SelectionColor = Color.Black;
                        break;
                }
            }
            catch (Exception e)
            {
                e.ToString();
            }
            textLOG.SelectionStart = textLOG.Text.Length;
            textLOG.ScrollToCaret();
            //Application.DoEvents();
        }
        private void General_timer_Handler(object sender, ElapsedEventArgs e)
        {
            if (!serialport.IsOpen)
            {
                BeginInvoke(new Delegate_void_NoPam(Update_COM));
            }

        }
        private void Change_Change_RichTextBox(RichTextBox textbox, Color cor, string texto)
        {
            try
            {
                textbox.BackColor = cor;
                textbox.Text = texto;
            }
            catch
            { }
        }

        /// <summary>
        /// Função que buscar novas COM disponíves se não houver nenhuma já aberta
        /// </summary>
        private void Update_COM()
        {
            try
            {
                /* Testa se há COM aberta */
                if (!serialport.IsOpen)
                {
                    /* Procura as COMs disponíveis */
                    foreach (string str in SerialPort.GetPortNames())
                    {
                        /* Testa se a COM str já não está na lista */
                        if (com_port.FindString(str) == -1)
                        {
                            /* Adiciona a COM str na lista */
                            com_port.Items.Add(str);
                        }
                    }
                    /* Se alguma COM foi encontrada, seleciona a primeira da lista */
                    //if (com_port.Items.Count > 0)
                    //{
                    //    com_port.SelectedIndex = 0;
                    //    com_baud.SelectedIndex = 1;
                    //}
                }
            }
            catch (Exception e)
            {
                MessageBox.Show(e.ToString());
            }
        }

        private void button_com_Click(object sender, EventArgs e)
        {
            /* Testa se o botão deve abrir ou fechar a porta*/
            switch (button_com.Text)
            {
                case "Abrir Porta":
                    /* Testa se a com está aberta */
                    if (serialport.IsOpen)
                    {
                        serialport.Close();
                    }
                    try
                    {
                        if (com_port.Text == "" || com_baud.Text == "")
                        {
                            MessageBox.Show("A porta não pode ser iniciada com parâmetros em branco.", "Atenção");
                            return;
                        }
                        /* Configura a COM conforme as configurações inseridas na tela inicial */
                        #region Configuração de Porta
                        serialport.PortName = com_port.Text;
                        serialport.BaudRate = int.Parse(com_baud.Text);
                        serialport.Parity = Parity.None;
                        serialport.DataBits = 8;
                        serialport.StopBits = StopBits.One;
                        serialport.DataReceived += new SerialDataReceivedEventHandler(Serialport_DataReceived);
                        #endregion
                        /* Abre a comunicação */
                        serialport.Open();
                        /* Testa se a porta for aberta, e realiza as configurações se estiver ok */
                        if (serialport.IsOpen)
                        {
                            if (!textLOG.Enabled)
                            {
                                textLOG.Enabled = true;
                            }
                            BeginInvoke(new Delegate_void_String(Escreve_textLOG), new object[] { "", string.Format("Porta Aberta. {0}. Baudrate: {1}", com_port.Text, com_baud.Text) });
                            //BeginInvoke(new Delegate_void_Serial_bytearray(Enviar_Serial), new object[] {Mensagem.Ler_Acelerometro(0x02, 0x55, 0x00)});
                            button_com.Text = "Fechar Porta";
                        }
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.ToString());
                    }
                    break;

                case "Fechar Porta":
                    /* Testa se a com está aberta */

                    try
                    {
                        serialport.Close();
                        if (!serialport.IsOpen)
                        {
                            button_com.Text = "Abrir Porta";
                        }
                    }
                    catch (Exception ex)
                    {
                        MessageBox.Show(ex.ToString());
                    }

                    break;
            }
        }

        private void Serialport_DataReceived(object sender, SerialDataReceivedEventArgs e)
        {
            try
            {
                SerialPort Serial = (SerialPort)sender;
                //Thread.Sleep(50);
                int bytes = Serial.BytesToRead;
                byte[] recebido = new byte[bytes];
                Serial.Read(recebido, 0, bytes);
                if (recebido == null || recebido.Length != 8) { }
                else
                {
                    Handle_Frame(recebido);
                }
            }
            catch { }

        }

        private void Enviar_Serial(byte[] msg)
        {
            try
            {
                if (serialport.IsOpen)
                {
                    serialport.Write(msg, 0, msg.Length);
                    BeginInvoke(new Delegate_void_String(Escreve_textLOG), new object[] { "", string.Format("TX: {0}", BitConverter.ToString(msg).Replace('-', ' ')) });
                }
            }
            catch (Exception ex)
            {
            }
        }

        public void Find_IP()
        {
            try
            {
                #region Get External IP
                /* string externalip = new WebClient().DownloadString("http://icanhazip.com");
               externalip = new System.Net.WebClient().DownloadString("http://bot.whatismyipaddress.com");
               externalip = new System.Net.WebClient().DownloadString("http://ipinfo.io/ip");
               externalip = externalip.Trim();
               textIP.Text = externalip;    */
                #endregion
                #region Get IPV4
                string IP4Address = String.Empty;

                foreach (IPAddress IPA in Dns.GetHostAddresses(Dns.GetHostName()))
                {
                    if (IPA.AddressFamily == AddressFamily.InterNetwork)
                    {
                        IP4Address = IPA.ToString();
                        break;
                    }
                }
#if (!TESTE)
                tcp_ip.Text = IP4Address;
                tcp_port.Text = "5001";
#else
                tcp_ip.Text = "127.0.0.1";
                tcp_port.Text = "5001";
#endif
                #endregion
            }
            catch { }
        }

        private void button_tcp_Click(object sender, EventArgs e)
        {
            try
            {
                switch (button_tcp.Text)
                {
                    case "Abrir Servidor":
                        if (tcp_ip.Text == "" || tcp_port.Text == "")
                        {
                            MessageBox.Show("O servidor TCP/IP não pode ser iniciado com parâmetros em branco.", "Atenção");
                            return;
                        }
                        if (flag_status_servidorTCP == 0x00)
                        {
                            servidorTCP = new TcpListener(IPAddress.Parse(tcp_ip.Text), Int32.Parse(tcp_port.Text));
                            servidorTCP.Start();

                            servidorTCP.BeginAcceptTcpClient(OnClientAccepted, servidorTCP);
                            textLOG.Enabled = true;
                            flag_status_servidorTCP = 0x01;
                            button_tcp.Text = "Fechar Servidor";
                            BeginInvoke(new Delegate_void_String(Escreve_textLOG), new object[] { "", "Servidor TCP/IP Aberto. Aguardando conexão de clientes" });
                        }
                        break;

                    default:
                        flag_status_servidorTCP = 0x00;
                        servidorTCP.Stop();

                        button_tcp.Text = "Abrir Servidor";
                        break;
                }

            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.ToString());
            }
        }

        private void tabControl_SelectedIndexChanged(object sender, EventArgs e)
        {
            if (tabControl.SelectedIndex == 1)
            {

                WindowState = FormWindowState.Maximized; // TODO
            }
        }

        private void OnClientAccepted(IAsyncResult ar)
        {
            TcpListener listener = ar.AsyncState as TcpListener;
            servidorTCP = listener;
            if (listener == null)
                return;

            try
            {
                TcpClient clientTCP = listener.EndAcceptTcpClient(ar);
                NetworkStream stream = clientTCP.GetStream();
                byte[] buffer = new byte[100];
                stream.BeginRead(buffer, 0, buffer.Length, OnClientRead, clientTCP);
                BeginInvoke(new Delegate_void_String(Escreve_textLOG), new object[] { "", "Cliente Conectado" });
                listener.BeginAcceptTcpClient(OnClientAccepted, listener);
            }
            catch { }
            finally
            {
                //listener.BeginAcceptTcpClient(OnClientAccepted, listener);
            }
        }

        private void OnClientRead(IAsyncResult ar)
        {
            TcpClient client = ar.AsyncState as TcpClient;
            byte[] bytes = new byte[100];
            NetworkStream stream = client.GetStream();
            //long size = stream.Length;
            int i;
            bool dataAvailable = false;
            // Loop to receive all the data sent by the client.

            OnMessageReceived(bytes);
            while (true && client.Connected && (flag_status_servidorTCP == 0x00))
            {
                if (!dataAvailable)
                {
                    dataAvailable = stream.DataAvailable;
                }

                if (dataAvailable)
                {
                    i = stream.Read(bytes, 0, bytes.Length);//) != 0)
                    OnMessageReceived(bytes);
                    dataAvailable = false;
                }

                /*if (servidorTCP.Pending())
                {
                    Console.WriteLine("found new client");
                    break;
                }*/
            }

            Console.WriteLine("Client close");
            // Shutdown and end connection
            try
            {
                if (stream != null)
                {
                    stream.Dispose();
                }

                client.Close();
            }
            catch (Exception ex)
            {
                ex.ToString();
            }
        }


        private void OnMessageReceived(byte[] recebido)
        {
            // process the message here
            if (recebido == null || recebido.Length == 0) { }
            else
            {
                try
                {
                    /*if (!bw_message_TCP1.IsBusy)
                    {
                        bw_message_TCP1.RunWorkerAsync(recebido);
                    }
                    else if (!bw_message_TCP2.IsBusy)
                    {
                        bw_message_TCP2.RunWorkerAsync(recebido);
                    }
                    else if (!bw_message_TCP3.IsBusy)
                    {
                        bw_message_TCP3.RunWorkerAsync(recebido);
                    }
                    */
                    ListaHandleMSG.Add(recebido);
                    if (bw_message.IsBusy == false)
                    {
                        bw_message.RunWorkerAsync();
                    }
                }
                catch (Exception ex)
                {
                    MessageBox.Show(ex.ToString());
                }
            }
        }
        
        #region Tratamento de Dados

        void Handle_Frame(byte[] received_frame)
        {
            byte[] aux = new byte[received_frame.Length];
            byte[] byte_aux;
            string s;
            // Realiza uma cópia do frame recebido para não perder o original
            Array.Copy(received_frame, aux, aux.Length);

            // se o frame não for válido, ele não será manipulado
            // já retira os bytes de escape dentro da função




            // Testar se o receptor é o Servidor. Se não for, encaminhar para destinatário.
            Int16 dados = 0;

            dados = (Int16)((aux[1] << 8) | (aux[2]));
            BeginInvoke(new Delegate_void_Handle_Array(Handle_SensorTemperatura), new object[] { dados });

            dados = (Int16)((aux[3] << 8) | (aux[4]));
            BeginInvoke(new Delegate_void_Handle_Array(Handle_SensorTemperatura2), new object[] { dados });

            dados = (Int16)((aux[5] << 8) | (aux[6]));
            BeginInvoke(new Delegate_void_Handle_Array(Handle_SensorTemperatura3), new object[] { dados });

        }

        void Handle_Table(byte[] received_frame)
        {
            if (received_frame.Length <= 0) { return; }
            byte ID = received_frame[1];
            string ID_str = ID.ToString();

            if (!ListaID_TX.Contains(ID))
            {
                #region Criar Tabela
                ListaID_TX.Add(ID);
                FilesOnFolder = Directory.GetFiles(FolderPath);
                TabelaExcel = new FileInfo(FolderPath + string.Format("\\Leitura de Sensores ID {0}.xlsx", ID_str));
                using (var package = new ExcelPackage(TabelaExcel))
                {
                    ExcelWorkbook workBook = package.Workbook;
                    ExcelWorksheet currentWorksheet1 = null;
                    ExcelWorksheet currentWorksheet2 = null;
                    ExcelWorksheet currentWorksheet3 = null;
                    ExcelWorksheet currentWorksheet4 = null;
                    ExcelWorksheet currentWorksheet5 = null;
                    ExcelWorksheet currentWorksheet6 = null;
                    ExcelWorksheet currentWorksheet7 = null;
                    ExcelWorksheet currentWorksheet8 = null;
                    workBook.Worksheets.Add("Acelerômetro");
                    package.Workbook.Worksheets.Add("Temperatura");
                    package.Workbook.Worksheets.Add("Corrente");
                    package.Workbook.Worksheets.Add("Ultrassônico");
                    package.Workbook.Worksheets.Add("Magnetômetro");
                    package.Workbook.Worksheets.Add("RTC");
                    package.Workbook.Worksheets.Add("Portas Digitais");
                    package.Workbook.Worksheets.Add("Portas Analógicas");
                    currentWorksheet1 = workBook.Worksheets.SingleOrDefault(w => w.Name == "Acelerômetro");
                    currentWorksheet2 = workBook.Worksheets.SingleOrDefault(w => w.Name == "Temperatura");
                    currentWorksheet3 = workBook.Worksheets.SingleOrDefault(w => w.Name == "Corrente");
                    currentWorksheet4 = workBook.Worksheets.SingleOrDefault(w => w.Name == "Ultrassônico");
                    currentWorksheet5 = workBook.Worksheets.SingleOrDefault(w => w.Name == "Magnetômetro");
                    currentWorksheet6 = workBook.Worksheets.SingleOrDefault(w => w.Name == "RTC");
                    currentWorksheet7 = workBook.Worksheets.SingleOrDefault(w => w.Name == "Portas Digitais");
                    currentWorksheet8 = workBook.Worksheets.SingleOrDefault(w => w.Name == "Portas Analógicas");
                    for (int i = 0; i < 256; i++)
                    {
                        currentWorksheet1.Cells[1, 1 + (6 * i)].Value = "Data";
                        currentWorksheet1.Cells[1, 2 + (6 * i)].Value = "Hora";
                        currentWorksheet1.Cells[1, 3 + (6 * i)].Value = "ID do Sensor";
                        currentWorksheet1.Cells[1, 4 + (6 * i)].Value = "Eixo X";
                        currentWorksheet1.Cells[1, 5 + (6 * i)].Value = "Eixo Y";
                        currentWorksheet1.Cells[1, 6 + (6 * i)].Value = "Eixo Z";

                        currentWorksheet2.Cells[1, 1 + (4 * i)].Value = "Data";
                        currentWorksheet2.Cells[1, 2 + (4 * i)].Value = "Hora";
                        currentWorksheet2.Cells[1, 3 + (4 * i)].Value = "ID do Sensor";
                        currentWorksheet2.Cells[1, 4 + (4 * i)].Value = "Temperatura";

                        currentWorksheet3.Cells[1, 1 + (4 * i)].Value = "Data";
                        currentWorksheet3.Cells[1, 2 + (4 * i)].Value = "Hora";
                        currentWorksheet3.Cells[1, 3 + (4 * i)].Value = "ID do Sensor";
                        currentWorksheet3.Cells[1, 4 + (4 * i)].Value = "Corrente";

                        currentWorksheet4.Cells[1, 1 + (4 * i)].Value = "Data";
                        currentWorksheet4.Cells[1, 2 + (4 * i)].Value = "Hora";
                        currentWorksheet4.Cells[1, 3 + (4 * i)].Value = "ID do Sensor";
                        currentWorksheet4.Cells[1, 4 + (4 * i)].Value = "Distância";

                        currentWorksheet5.Cells[1, 1 + (6 * i)].Value = "Data";
                        currentWorksheet5.Cells[1, 2 + (6 * i)].Value = "Hora";
                        currentWorksheet5.Cells[1, 3 + (6 * i)].Value = "ID do Sensor";
                        currentWorksheet5.Cells[1, 4 + (6 * i)].Value = "Eixo X";
                        currentWorksheet5.Cells[1, 5 + (6 * i)].Value = "Eixo Y";
                        currentWorksheet5.Cells[1, 6 + (6 * i)].Value = "Eixo Z";

                        currentWorksheet6.Cells[1, 1 + (5 * i)].Value = "Data";
                        currentWorksheet6.Cells[1, 2 + (5 * i)].Value = "Hora";
                        currentWorksheet6.Cells[1, 3 + (5 * i)].Value = "ID do Sensor";
                        currentWorksheet6.Cells[1, 4 + (5 * i)].Value = "Data do Sensor";
                        currentWorksheet6.Cells[1, 5 + (5 * i)].Value = "Horário do Sensor";

                        currentWorksheet7.Cells[1, 1 + (4 * i)].Value = "Data";
                        currentWorksheet7.Cells[1, 2 + (4 * i)].Value = "Hora";
                        currentWorksheet7.Cells[1, 3 + (4 * i)].Value = "ID do Sensor";
                        currentWorksheet7.Cells[1, 4 + (4 * i)].Value = "Valor da Porta";

                        currentWorksheet8.Cells[1, 1 + (4 * i)].Value = "Data";
                        currentWorksheet8.Cells[1, 2 + (4 * i)].Value = "Hora";
                        currentWorksheet8.Cells[1, 3 + (4 * i)].Value = "ID do Sensor";
                        currentWorksheet8.Cells[1, 4 + (4 * i)].Value = "Valor da Porta";
                    }
                    package.Save();
                }
                #endregion

            }
            else
            {

            }
        }

        void Handle_SensorTemperatura(Int16 array)
        {
            string aux;
            double a;
            a = Convert.ToDouble(array);
            a = (a * 19.6) / 32767;
            if (cartesian_STemp.Series[0].Values.Count > 100)
            {
                cartesian_STemp.Series[0].Values.RemoveAt(0);
                double[] temp = new double[100];
            }
            cartesian_STemp.Series[0].Values.Add(a);

        }
        void Handle_SensorTemperatura2(Int16 array)
        {
            string aux;
            double a;
            a = Convert.ToDouble(array);
            a = (a * 0.02) - 273.15;
            if (cartesian_STemp2.Series[0].Values.Count > 100)
            {
                cartesian_STemp2.Series[0].Values.RemoveAt(0);
                double[] temp = new double[100];
            }
            cartesian_STemp2.Series[0].Values.Add(a);

        }

        void Handle_SensorTemperatura3(Int16 array)
        {
            string aux;
            double a;
            a = Convert.ToDouble(array);
            a = (a * 0.02) - 273.15;
            if (cartesian_STemp3.Series[0].Values.Count > 100)
            {
                cartesian_STemp3.Series[0].Values.RemoveAt(0);
                double[] temp = new double[100];
            }
            cartesian_STemp3.Series[0].Values.Add(a);

        }

        #endregion

        private void buttonCleanLOG_Click(object sender, EventArgs e)
        {
            if (DialogResult.Yes == MessageBox.Show("Deseja apagar o LOG de mensagens?", "Atenção", MessageBoxButtons.YesNo))
            {
                textLOG.Clear();
            }
        }

        private void buttonSaveLOG_Click(object sender, EventArgs e)
        {
            if (DialogResult.Yes == MessageBox.Show("Deseja salvar o LOG de respostas?", "Atenção", MessageBoxButtons.YesNo))
            {
                SaveFileDialog saveFileDialog1 = new SaveFileDialog();
                saveFileDialog1.DefaultExt = "*.txt";
                saveFileDialog1.Filter = "txt files | *.txt";
                saveFileDialog1.FileName = "LOG de Mensagens";
                // aguarda o usuário decidir onde salvará o arquivo
                try
                {
                    if (saveFileDialog1.ShowDialog() == DialogResult.OK)
                    {
                        //salva o arquivo
                        textLOG.SaveFile(saveFileDialog1.FileName, RichTextBoxStreamType.UnicodePlainText);
                        MessageBox.Show("Arquivo Salvo");

                    }
                    else
                    {
                        MessageBox.Show("Arquivo não foi salvo");
                    }
                }
                catch
                {
                    // avisa caso ocorra algum erro
                    MessageBox.Show("Falha");
                }
            }
        }
    }
    public class Protocolo
    {   /*SOF - SND - RCP - DSZ - CMD - PRTMT1 - PRTMT2 - PRMTn - CRCmsb - CRClsb - EOF*/
        public Protocolo()
        {
            Lista_Enderecos.Add(SCADA);
        }
        #region Constantes do Protocolo
        private byte SOF = 0x01;
        public List<byte> Lista_Enderecos = new List<byte>();
        private byte EOF = 0x04;
        private byte ESCAPE = 0x10;
        public const byte SCADA = 0x00;
        #endregion

        #region Sensores
        public byte Acelerometro = 0x01;
        public byte SensorTemperatura = 0x02;
        public byte SensorCorrente = 0x03;
        public byte SensorUltrassonico = 0x04;
        public byte Magnetometro = 0x05;
        public byte RTC = 0x06;
        public byte PortaDigital = 0x07;
        public byte PortaAnalogica = 0x08;
        #endregion

        #region Functions para frames
        ulong CRC_Calc(byte[] msg)
        { //calcula o CRC do frame, e retorna valor de 16b
            ushort newchar;
            int test;
            int crcword;
            crcword = 0xffff;

            //int i = 1;
            for (int i = 1; i < (msg.Length - 3); i++)
            {
                newchar = msg[i];
                for (int j = 0; j < 8; j++)
                {
                    test = (ushort)(newchar & 0x00FF);
                    test = (ushort)(test << (j + 8));
                    test = (ushort)(test ^ crcword);
                    test = (ushort)(test & 0x8000);

                    if (test != 0)
                    {
                        crcword = (ushort)(crcword << 1);
                        crcword = (ushort)(crcword ^ 0x1021);
                    }
                    else
                    {
                        crcword = (ushort)(crcword << 1);
                    }
                }
            }
            return (ushort)crcword;
        }
        /// <summary>
        /// Valida o frame passado. São comparados os valores de SOF e EOF, retirados os Escape Bytes e calculado o CRC. O frame passado não é alterado, é utilizada uma cópia interna.
        /// </summary>
        /// <param name="msg">Vetor de bytes a ser validado</param>
        /// <returns>Retorna se o frame é válido ou não (bool).</returns>
        public bool Frame_validation(ref byte[] msg)
        {


            ulong crc;
            int index_sof = 0;
            int index_EOF = 0;
            for (int i = 0; i < msg.Length; i++)
            {
                if (msg[i] == 0x01)
                {
                    index_sof = i;
                }
                if (msg[i] == 0x04)
                {
                    index_EOF = i;
                    break;
                }
            }
            byte[] aux = new byte[index_EOF - index_sof + 1];
            Array.Copy(msg, index_sof, aux, 0, aux.Length);
            if ((aux[index_sof] != SOF) || (aux[index_EOF] != EOF))
            {
                return false;
            }
            Array.Copy(msg, aux, aux.Length);
            Escape_Bytes(ref aux, 0x01);

            crc = (ulong)((aux[aux.Length - 3] << 8) | (aux[aux.Length - 2]));
            if (crc == 0)
            {
                msg = new byte[aux.Length];
                Array.Copy(aux, msg, msg.Length);
                return true;
            }
            if (crc != CRC_Calc(aux))
            {
                return false;
            }

            Array.Copy(aux, msg, msg.Length);
            return true;
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="msg">Vetor de bytes no qual se deseja adicionar ou retirar bytes de escape.</param>
        /// <param name="tipo">Utilizar 0x00 para adicionar os bytes de escape em msg. Utilizar 0x01 para retirar os bytes de escape de msg.</param>
        /// <returns></returns>
        public void Escape_Bytes(ref byte[] msg, byte tipo)
        {
            int cnt_escape = 0;
            byte[] aux;
            switch (tipo)
            {
                case 0x00:
                    for (int i = 1; i < msg.Length - 1; i++)
                    {
                        if ((msg[i] == SOF) || (msg[i] == ESCAPE) || (msg[i] == EOF))
                        { cnt_escape++; }
                    }

                    aux = new byte[msg.Length + cnt_escape];

                    aux[0] = msg[0];

                    for (int i = 1, j = 1; i < msg.Length - 1; i++)
                    {
                        if ((msg[i] == SOF) || (msg[i] == ESCAPE) || (msg[i] == EOF))
                        {
                            aux[j++] = 0x10;
                            aux[j++] = (byte)(msg[i] + 0x20);
                        }
                        else
                        {
                            aux[j++] = msg[i];
                        }
                    }

                    aux[aux.Length - 1] = msg[msg.Length - 1];
                    msg = new byte[aux.Length];
                    Array.Copy(aux, msg, msg.Length);

                    break;

                case 0x01:
                    for (int i = 1; i < msg.Length - 1; i++)
                    {
                        if ((msg[i] == ESCAPE))
                        { cnt_escape++; }
                    }

                    aux = new byte[msg.Length - cnt_escape];

                    aux[0] = msg[0];

                    for (int i = 1, j = 1; i < msg.Length - 1; i++)
                    {
                        if ((msg[i] == ESCAPE))
                        {
                            aux[j++] = (byte)(msg[++i] - 0x20);
                        }
                        else
                        {
                            aux[j++] = msg[i];
                        }
                    }

                    aux[aux.Length - 1] = msg[msg.Length - 1];
                    msg = new byte[aux.Length];
                    Array.Copy(aux, msg, msg.Length);

                    break;
            }


        }

        byte[] Montar_Frame(byte receptor, byte[] cmd)
        {
            byte[] rtrn = new byte[7 + cmd.Length];
            ulong crc_calc;
            rtrn[0] = SOF;
            rtrn[1] = (byte)Lista_Enderecos[0];
            rtrn[2] = receptor;
            rtrn[3] = (byte)(cmd.Length - 1);
            Array.Copy(cmd, 0, rtrn, 4, cmd.Length);
            crc_calc = CRC_Calc(rtrn);
            rtrn[rtrn.Length - 3] = (byte)((crc_calc >> 8) & 0xFF);
            rtrn[rtrn.Length - 2] = (byte)(crc_calc & 0xFF);
            rtrn[rtrn.Length - 1] = EOF;
            Escape_Bytes(ref rtrn, 0x00);
            return rtrn;
        }
        #endregion

        #region Funções
        /// <summary>
        /// Envia comando requisitando leitura de um acelerômetro
        /// </summary>
        /// <param name="RCP">Endereço do receptor</param>
        /// <param name="ID">Número de ID do acelerômetro</param>
        /// <param name="Eixo">Eixo que deve ser retornado valor. 0x00 - Todos. 0x01 - Eixo X. 0x02 - Eixo Y. 0x03 - Eixo Z.</param>
        /// <returns></returns>
        public byte[] Ler_Acelerometro(byte RCP, byte ID, byte Eixo)
        {
            byte[] CMD = { Acelerometro, ID, Eixo };
            return Montar_Frame(RCP, CMD);
        }
        //public byte[] Ler_TempMicro(byte RCP, byte ID)
        //{
        //    byte[] CMD = { SensorTempMicro, ID };
        //    return Montar_Frame(RCP, CMD);
        //}
        public byte[] Ler_Temperatura(byte RCP, byte ID)
        {
            byte[] CMD = { SensorTemperatura, ID };
            return Montar_Frame(RCP, CMD);
        }
        public byte[] Ler_SensorCorrente(byte RCP, byte ID)
        {
            byte[] CMD = { SensorCorrente, ID };
            return Montar_Frame(RCP, CMD);
        }

        public byte[] Ler_SUltra(byte RCP, byte ID)
        {
            byte[] CMD = { SensorUltrassonico, ID };
            return Montar_Frame(RCP, CMD);
        }

        public byte[] Enviar_RTC(byte RCP, byte ID, int tipo)
        {
            byte[] CMD = new byte[] { RTC, ID, (byte)tipo };
            string s;
            int a = 0;
            byte[] date = new byte[9];
            date[0] = (byte)DateTime.Now.DayOfWeek;
            date[1] = (byte)(DateTime.Now.Month - 1);
            date[2] = (byte)(DateTime.Now.Year - 2000);
            s = DateTime.Now.ToString("hh");
            date[3] = (byte)((int.Parse(s) >> 8) & 0xFF);
            date[4] = (byte)((int.Parse(s) >> 0) & 0xFF);
            s = DateTime.Now.ToString("mm");
            date[5] = (byte)((int.Parse(s) >> 8) & 0xFF);
            date[6] = (byte)((int.Parse(s) >> 0) & 0xFF);
            s = DateTime.Now.ToString("ss");
            date[7] = (byte)((int.Parse(s) >> 8) & 0xFF);
            date[8] = (byte)((int.Parse(s) >> 0) & 0xFF);
            switch (tipo)
            {
                case 0x01:
                    CMD = new byte[12];
                    CMD[0] = RTC;
                    CMD[1] = ID;
                    CMD[2] = (byte)tipo;
                    Array.Copy(date, 0, CMD, 3, date.Length);
                    break;
            }
            return Montar_Frame(RCP, CMD);
        }

        public byte[] Enviar_Analogico(byte RCP, byte ID, int tipo, UInt16 valor)
        {
            byte[] CMD = null;
            if (tipo == 1)
            {
                CMD = new byte[] { PortaAnalogica, ID, (byte)tipo, (byte)((valor >> 8) & 0xff), (byte)(valor & 0xFF) };
            }
            else
            {
                CMD = new byte[] { PortaAnalogica, ID, (byte)tipo };
            }
            return Montar_Frame(RCP, CMD);
        }

        public byte[] Enviar_GPIO(byte RCP, byte ID, int tipo)
        {
            byte[] CMD = { PortaDigital, ID, (byte)tipo };
            return Montar_Frame(RCP, CMD);
        }
        #endregion
    }
}
