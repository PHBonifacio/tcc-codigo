﻿namespace Sistema_Supervisorio
{
    partial class ControlSystem
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ControlSystem));
            this.groupBoxTCP = new System.Windows.Forms.GroupBox();
            this.button_tcp = new System.Windows.Forms.Button();
            this.tcp_port = new System.Windows.Forms.TextBox();
            this.label2 = new System.Windows.Forms.Label();
            this.tcp_ip = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.groupBoxSerial = new System.Windows.Forms.GroupBox();
            this.button_com = new System.Windows.Forms.Button();
            this.com_baud = new System.Windows.Forms.ComboBox();
            this.label4 = new System.Windows.Forms.Label();
            this.com_port = new System.Windows.Forms.ComboBox();
            this.label3 = new System.Windows.Forms.Label();
            this.tabControl = new System.Windows.Forms.TabControl();
            this.tab_configuracao = new System.Windows.Forms.TabPage();
            this.groupBox13 = new System.Windows.Forms.GroupBox();
            this.button1 = new System.Windows.Forms.Button();
            this.check_JSON = new System.Windows.Forms.CheckBox();
            this.tab_log = new System.Windows.Forms.TabPage();
            this.buttonSaveLOG = new System.Windows.Forms.Button();
            this.buttonCleanLOG = new System.Windows.Forms.Button();
            this.textLOG = new System.Windows.Forms.RichTextBox();
            this.tab_Temperatura_2 = new System.Windows.Forms.TabPage();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.text_Temperatura_Inst = new System.Windows.Forms.TextBox();
            this.label19 = new System.Windows.Forms.Label();
            this.text_ID_STemp = new System.Windows.Forms.TextBox();
            this.label18 = new System.Windows.Forms.Label();
            this.text_TX_ID_STemp = new System.Windows.Forms.TextBox();
            this.label16 = new System.Windows.Forms.Label();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.cartesian_STemp = new LiveCharts.WinForms.CartesianChart();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.text_STemp_TX_ID = new System.Windows.Forms.TextBox();
            this.button_STemp_Enviar = new System.Windows.Forms.Button();
            this.text_STemp_ID = new System.Windows.Forms.TextBox();
            this.label15 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.tab_temperatura_1 = new System.Windows.Forms.TabPage();
            this.groupBox7 = new System.Windows.Forms.GroupBox();
            this.cartesian_STemp2 = new LiveCharts.WinForms.CartesianChart();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.text_Corrente_Inst = new System.Windows.Forms.TextBox();
            this.label20 = new System.Windows.Forms.Label();
            this.text_ID_SCorrente = new System.Windows.Forms.TextBox();
            this.label21 = new System.Windows.Forms.Label();
            this.text_TX_ID_SCorrente = new System.Windows.Forms.TextBox();
            this.label22 = new System.Windows.Forms.Label();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.text_SCorrente_RCP_ID = new System.Windows.Forms.TextBox();
            this.button_SCorrente_Enviar = new System.Windows.Forms.Button();
            this.text_SCorrente_ID = new System.Windows.Forms.TextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.tabPage1 = new System.Windows.Forms.TabPage();
            this.cartesian_STemp3 = new LiveCharts.WinForms.CartesianChart();
            this.groupBoxTCP.SuspendLayout();
            this.groupBoxSerial.SuspendLayout();
            this.tabControl.SuspendLayout();
            this.tab_configuracao.SuspendLayout();
            this.groupBox13.SuspendLayout();
            this.tab_log.SuspendLayout();
            this.tab_Temperatura_2.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.groupBox2.SuspendLayout();
            this.tab_temperatura_1.SuspendLayout();
            this.groupBox7.SuspendLayout();
            this.groupBox6.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.tabPage1.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBoxTCP
            // 
            this.groupBoxTCP.Controls.Add(this.button_tcp);
            this.groupBoxTCP.Controls.Add(this.tcp_port);
            this.groupBoxTCP.Controls.Add(this.label2);
            this.groupBoxTCP.Controls.Add(this.tcp_ip);
            this.groupBoxTCP.Controls.Add(this.label1);
            this.groupBoxTCP.Location = new System.Drawing.Point(6, 6);
            this.groupBoxTCP.Name = "groupBoxTCP";
            this.groupBoxTCP.Size = new System.Drawing.Size(148, 166);
            this.groupBoxTCP.TabIndex = 1;
            this.groupBoxTCP.TabStop = false;
            this.groupBoxTCP.Text = "Comunicação TCP/IP";
            // 
            // button_tcp
            // 
            this.button_tcp.Location = new System.Drawing.Point(9, 97);
            this.button_tcp.Name = "button_tcp";
            this.button_tcp.Size = new System.Drawing.Size(121, 54);
            this.button_tcp.TabIndex = 4;
            this.button_tcp.Text = "Abrir Servidor";
            this.button_tcp.UseVisualStyleBackColor = true;
            this.button_tcp.Click += new System.EventHandler(this.button_tcp_Click);
            // 
            // tcp_port
            // 
            this.tcp_port.Location = new System.Drawing.Point(9, 71);
            this.tcp_port.Name = "tcp_port";
            this.tcp_port.Size = new System.Drawing.Size(121, 20);
            this.tcp_port.TabIndex = 3;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Location = new System.Drawing.Point(6, 55);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(35, 13);
            this.label2.TabIndex = 2;
            this.label2.Text = "Porta:";
            // 
            // tcp_ip
            // 
            this.tcp_ip.Location = new System.Drawing.Point(9, 32);
            this.tcp_ip.Name = "tcp_ip";
            this.tcp_ip.Size = new System.Drawing.Size(121, 20);
            this.tcp_ip.TabIndex = 1;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(6, 16);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(20, 13);
            this.label1.TabIndex = 0;
            this.label1.Text = "IP:";
            // 
            // groupBoxSerial
            // 
            this.groupBoxSerial.Controls.Add(this.button_com);
            this.groupBoxSerial.Controls.Add(this.com_baud);
            this.groupBoxSerial.Controls.Add(this.label4);
            this.groupBoxSerial.Controls.Add(this.com_port);
            this.groupBoxSerial.Controls.Add(this.label3);
            this.groupBoxSerial.Location = new System.Drawing.Point(160, 6);
            this.groupBoxSerial.Name = "groupBoxSerial";
            this.groupBoxSerial.Size = new System.Drawing.Size(146, 166);
            this.groupBoxSerial.TabIndex = 2;
            this.groupBoxSerial.TabStop = false;
            this.groupBoxSerial.Text = "Comunicação Serial";
            // 
            // button_com
            // 
            this.button_com.Location = new System.Drawing.Point(9, 99);
            this.button_com.Name = "button_com";
            this.button_com.Size = new System.Drawing.Size(121, 54);
            this.button_com.TabIndex = 5;
            this.button_com.Text = "Abrir Porta";
            this.button_com.UseVisualStyleBackColor = true;
            this.button_com.Click += new System.EventHandler(this.button_com_Click);
            // 
            // com_baud
            // 
            this.com_baud.FormattingEnabled = true;
            this.com_baud.Items.AddRange(new object[] {
            "9600",
            "19200",
            "38400",
            "57600",
            "115200"});
            this.com_baud.Location = new System.Drawing.Point(9, 72);
            this.com_baud.Name = "com_baud";
            this.com_baud.Size = new System.Drawing.Size(121, 21);
            this.com_baud.TabIndex = 3;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(6, 56);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(53, 13);
            this.label4.TabIndex = 2;
            this.label4.Text = "Baudrate:";
            // 
            // com_port
            // 
            this.com_port.FormattingEnabled = true;
            this.com_port.Location = new System.Drawing.Point(9, 32);
            this.com_port.Name = "com_port";
            this.com_port.Size = new System.Drawing.Size(121, 21);
            this.com_port.TabIndex = 1;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Location = new System.Drawing.Point(6, 16);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(64, 13);
            this.label3.TabIndex = 0;
            this.label3.Text = "Porta Serial:";
            // 
            // tabControl
            // 
            this.tabControl.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tabControl.Controls.Add(this.tab_configuracao);
            this.tabControl.Controls.Add(this.tab_log);
            this.tabControl.Controls.Add(this.tab_Temperatura_2);
            this.tabControl.Controls.Add(this.tab_temperatura_1);
            this.tabControl.Controls.Add(this.tabPage1);
            this.tabControl.Location = new System.Drawing.Point(12, 12);
            this.tabControl.Name = "tabControl";
            this.tabControl.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.tabControl.SelectedIndex = 0;
            this.tabControl.Size = new System.Drawing.Size(1288, 582);
            this.tabControl.TabIndex = 3;
            this.tabControl.SelectedIndexChanged += new System.EventHandler(this.tabControl_SelectedIndexChanged);
            // 
            // tab_configuracao
            // 
            this.tab_configuracao.BackColor = System.Drawing.SystemColors.Control;
            this.tab_configuracao.Controls.Add(this.groupBox13);
            this.tab_configuracao.Controls.Add(this.groupBoxSerial);
            this.tab_configuracao.Controls.Add(this.groupBoxTCP);
            this.tab_configuracao.Location = new System.Drawing.Point(4, 22);
            this.tab_configuracao.Name = "tab_configuracao";
            this.tab_configuracao.Padding = new System.Windows.Forms.Padding(3);
            this.tab_configuracao.Size = new System.Drawing.Size(1280, 556);
            this.tab_configuracao.TabIndex = 0;
            this.tab_configuracao.Text = "Configuração";
            // 
            // groupBox13
            // 
            this.groupBox13.Controls.Add(this.button1);
            this.groupBox13.Controls.Add(this.check_JSON);
            this.groupBox13.Location = new System.Drawing.Point(313, 7);
            this.groupBox13.Name = "groupBox13";
            this.groupBox13.Size = new System.Drawing.Size(143, 165);
            this.groupBox13.TabIndex = 3;
            this.groupBox13.TabStop = false;
            this.groupBox13.Text = "Configurações";
            // 
            // button1
            // 
            this.button1.Location = new System.Drawing.Point(7, 68);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(121, 23);
            this.button1.TabIndex = 1;
            this.button1.Text = "Criar Tabelas Excel";
            this.button1.UseVisualStyleBackColor = true;
            // 
            // check_JSON
            // 
            this.check_JSON.AutoSize = true;
            this.check_JSON.Location = new System.Drawing.Point(7, 34);
            this.check_JSON.Name = "check_JSON";
            this.check_JSON.Size = new System.Drawing.Size(121, 17);
            this.check_JSON.TabIndex = 0;
            this.check_JSON.Text = "Criar arquivos JSON";
            this.check_JSON.UseVisualStyleBackColor = true;
            // 
            // tab_log
            // 
            this.tab_log.BackColor = System.Drawing.SystemColors.Control;
            this.tab_log.Controls.Add(this.buttonSaveLOG);
            this.tab_log.Controls.Add(this.buttonCleanLOG);
            this.tab_log.Controls.Add(this.textLOG);
            this.tab_log.Location = new System.Drawing.Point(4, 22);
            this.tab_log.Name = "tab_log";
            this.tab_log.Padding = new System.Windows.Forms.Padding(3);
            this.tab_log.Size = new System.Drawing.Size(1280, 556);
            this.tab_log.TabIndex = 1;
            this.tab_log.Text = "Log de Comunicação";
            // 
            // buttonSaveLOG
            // 
            this.buttonSaveLOG.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonSaveLOG.Location = new System.Drawing.Point(1118, 3);
            this.buttonSaveLOG.Name = "buttonSaveLOG";
            this.buttonSaveLOG.Size = new System.Drawing.Size(75, 23);
            this.buttonSaveLOG.TabIndex = 2;
            this.buttonSaveLOG.Text = "Salvar LOG";
            this.buttonSaveLOG.UseVisualStyleBackColor = true;
            this.buttonSaveLOG.Click += new System.EventHandler(this.buttonSaveLOG_Click);
            // 
            // buttonCleanLOG
            // 
            this.buttonCleanLOG.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.buttonCleanLOG.Location = new System.Drawing.Point(1199, 3);
            this.buttonCleanLOG.Name = "buttonCleanLOG";
            this.buttonCleanLOG.Size = new System.Drawing.Size(75, 23);
            this.buttonCleanLOG.TabIndex = 1;
            this.buttonCleanLOG.Text = "Apagar LOG";
            this.buttonCleanLOG.UseVisualStyleBackColor = true;
            this.buttonCleanLOG.Click += new System.EventHandler(this.buttonCleanLOG_Click);
            // 
            // textLOG
            // 
            this.textLOG.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.textLOG.BackColor = System.Drawing.Color.White;
            this.textLOG.Enabled = false;
            this.textLOG.Location = new System.Drawing.Point(7, 36);
            this.textLOG.Name = "textLOG";
            this.textLOG.ReadOnly = true;
            this.textLOG.Size = new System.Drawing.Size(1267, 514);
            this.textLOG.TabIndex = 0;
            this.textLOG.Text = "";
            // 
            // tab_Temperatura_2
            // 
            this.tab_Temperatura_2.BackColor = System.Drawing.SystemColors.Control;
            this.tab_Temperatura_2.Controls.Add(this.groupBox4);
            this.tab_Temperatura_2.Controls.Add(this.groupBox3);
            this.tab_Temperatura_2.Controls.Add(this.groupBox2);
            this.tab_Temperatura_2.Location = new System.Drawing.Point(4, 22);
            this.tab_Temperatura_2.Name = "tab_Temperatura_2";
            this.tab_Temperatura_2.Size = new System.Drawing.Size(1280, 556);
            this.tab_Temperatura_2.TabIndex = 3;
            this.tab_Temperatura_2.Text = "Acelerômetro";
            // 
            // groupBox4
            // 
            this.groupBox4.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.groupBox4.Controls.Add(this.text_Temperatura_Inst);
            this.groupBox4.Controls.Add(this.label19);
            this.groupBox4.Controls.Add(this.text_ID_STemp);
            this.groupBox4.Controls.Add(this.label18);
            this.groupBox4.Controls.Add(this.text_TX_ID_STemp);
            this.groupBox4.Controls.Add(this.label16);
            this.groupBox4.Location = new System.Drawing.Point(3, 198);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(170, 350);
            this.groupBox4.TabIndex = 2;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Dados Recebidos";
            // 
            // text_Temperatura_Inst
            // 
            this.text_Temperatura_Inst.Font = new System.Drawing.Font("Microsoft Sans Serif", 36F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.text_Temperatura_Inst.Location = new System.Drawing.Point(10, 210);
            this.text_Temperatura_Inst.Multiline = true;
            this.text_Temperatura_Inst.Name = "text_Temperatura_Inst";
            this.text_Temperatura_Inst.Size = new System.Drawing.Size(154, 70);
            this.text_Temperatura_Inst.TabIndex = 13;
            this.text_Temperatura_Inst.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Location = new System.Drawing.Point(6, 194);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(93, 13);
            this.label19.TabIndex = 12;
            this.label19.Text = "Valor Instantâneo:";
            // 
            // text_ID_STemp
            // 
            this.text_ID_STemp.BackColor = System.Drawing.Color.White;
            this.text_ID_STemp.Font = new System.Drawing.Font("Microsoft Sans Serif", 36F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.text_ID_STemp.Location = new System.Drawing.Point(10, 121);
            this.text_ID_STemp.Multiline = true;
            this.text_ID_STemp.Name = "text_ID_STemp";
            this.text_ID_STemp.ReadOnly = true;
            this.text_ID_STemp.Size = new System.Drawing.Size(154, 70);
            this.text_ID_STemp.TabIndex = 11;
            this.text_ID_STemp.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Location = new System.Drawing.Point(7, 16);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(95, 13);
            this.label18.TabIndex = 4;
            this.label18.Text = "ID do Transmissor:";
            // 
            // text_TX_ID_STemp
            // 
            this.text_TX_ID_STemp.BackColor = System.Drawing.Color.White;
            this.text_TX_ID_STemp.Font = new System.Drawing.Font("Microsoft Sans Serif", 36F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.text_TX_ID_STemp.Location = new System.Drawing.Point(10, 32);
            this.text_TX_ID_STemp.Multiline = true;
            this.text_TX_ID_STemp.Name = "text_TX_ID_STemp";
            this.text_TX_ID_STemp.ReadOnly = true;
            this.text_TX_ID_STemp.Size = new System.Drawing.Size(154, 70);
            this.text_TX_ID_STemp.TabIndex = 10;
            this.text_TX_ID_STemp.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Location = new System.Drawing.Point(6, 105);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(150, 13);
            this.label16.TabIndex = 7;
            this.label16.Text = "ID do Sensor de Temperatura:";
            // 
            // groupBox3
            // 
            this.groupBox3.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox3.Controls.Add(this.cartesian_STemp);
            this.groupBox3.Location = new System.Drawing.Point(177, 4);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(1100, 550);
            this.groupBox3.TabIndex = 1;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Gráfico de Temperatura";
            // 
            // cartesian_STemp
            // 
            this.cartesian_STemp.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.cartesian_STemp.BackColor = System.Drawing.Color.White;
            this.cartesian_STemp.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cartesian_STemp.Location = new System.Drawing.Point(6, 20);
            this.cartesian_STemp.Name = "cartesian_STemp";
            this.cartesian_STemp.Size = new System.Drawing.Size(1088, 530);
            this.cartesian_STemp.TabIndex = 8;
            this.cartesian_STemp.Text = "chart_Temperatura";
            // 
            // groupBox2
            // 
            this.groupBox2.Controls.Add(this.text_STemp_TX_ID);
            this.groupBox2.Controls.Add(this.button_STemp_Enviar);
            this.groupBox2.Controls.Add(this.text_STemp_ID);
            this.groupBox2.Controls.Add(this.label15);
            this.groupBox2.Controls.Add(this.label14);
            this.groupBox2.Location = new System.Drawing.Point(4, 4);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(170, 190);
            this.groupBox2.TabIndex = 0;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "Comandos";
            // 
            // text_STemp_TX_ID
            // 
            this.text_STemp_TX_ID.Location = new System.Drawing.Point(10, 37);
            this.text_STemp_TX_ID.Name = "text_STemp_TX_ID";
            this.text_STemp_TX_ID.Size = new System.Drawing.Size(153, 20);
            this.text_STemp_TX_ID.TabIndex = 8;
            this.text_STemp_TX_ID.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // button_STemp_Enviar
            // 
            this.button_STemp_Enviar.Location = new System.Drawing.Point(10, 104);
            this.button_STemp_Enviar.Name = "button_STemp_Enviar";
            this.button_STemp_Enviar.Size = new System.Drawing.Size(153, 31);
            this.button_STemp_Enviar.TabIndex = 7;
            this.button_STemp_Enviar.Text = "Enviar Comando";
            this.button_STemp_Enviar.UseVisualStyleBackColor = true;
            // 
            // text_STemp_ID
            // 
            this.text_STemp_ID.Location = new System.Drawing.Point(10, 78);
            this.text_STemp_ID.Name = "text_STemp_ID";
            this.text_STemp_ID.Size = new System.Drawing.Size(153, 20);
            this.text_STemp_ID.TabIndex = 6;
            this.text_STemp_ID.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Location = new System.Drawing.Point(7, 61);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(72, 13);
            this.label15.TabIndex = 1;
            this.label15.Text = "ID do Sensor:";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Location = new System.Drawing.Point(7, 20);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(83, 13);
            this.label14.TabIndex = 0;
            this.label14.Text = "ID do Receptor:";
            // 
            // tab_temperatura_1
            // 
            this.tab_temperatura_1.BackColor = System.Drawing.SystemColors.Control;
            this.tab_temperatura_1.Controls.Add(this.groupBox7);
            this.tab_temperatura_1.Controls.Add(this.groupBox6);
            this.tab_temperatura_1.Controls.Add(this.groupBox5);
            this.tab_temperatura_1.Location = new System.Drawing.Point(4, 22);
            this.tab_temperatura_1.Name = "tab_temperatura_1";
            this.tab_temperatura_1.Size = new System.Drawing.Size(1280, 556);
            this.tab_temperatura_1.TabIndex = 4;
            this.tab_temperatura_1.Text = "Temperatura do Objeto";
            // 
            // groupBox7
            // 
            this.groupBox7.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox7.Controls.Add(this.cartesian_STemp2);
            this.groupBox7.Location = new System.Drawing.Point(179, 4);
            this.groupBox7.Name = "groupBox7";
            this.groupBox7.Size = new System.Drawing.Size(1100, 550);
            this.groupBox7.TabIndex = 2;
            this.groupBox7.TabStop = false;
            this.groupBox7.Text = "Gráfico de Corrente";
            // 
            // cartesian_STemp2
            // 
            this.cartesian_STemp2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.cartesian_STemp2.BackColor = System.Drawing.Color.White;
            this.cartesian_STemp2.Location = new System.Drawing.Point(6, 20);
            this.cartesian_STemp2.Name = "cartesian_STemp2";
            this.cartesian_STemp2.Size = new System.Drawing.Size(1088, 530);
            this.cartesian_STemp2.TabIndex = 0;
            // 
            // groupBox6
            // 
            this.groupBox6.Controls.Add(this.text_Corrente_Inst);
            this.groupBox6.Controls.Add(this.label20);
            this.groupBox6.Controls.Add(this.text_ID_SCorrente);
            this.groupBox6.Controls.Add(this.label21);
            this.groupBox6.Controls.Add(this.text_TX_ID_SCorrente);
            this.groupBox6.Controls.Add(this.label22);
            this.groupBox6.Location = new System.Drawing.Point(4, 200);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(170, 350);
            this.groupBox6.TabIndex = 1;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "Dados Recebidos";
            // 
            // text_Corrente_Inst
            // 
            this.text_Corrente_Inst.Font = new System.Drawing.Font("Microsoft Sans Serif", 36F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.text_Corrente_Inst.Location = new System.Drawing.Point(10, 210);
            this.text_Corrente_Inst.Multiline = true;
            this.text_Corrente_Inst.Name = "text_Corrente_Inst";
            this.text_Corrente_Inst.Size = new System.Drawing.Size(154, 70);
            this.text_Corrente_Inst.TabIndex = 19;
            this.text_Corrente_Inst.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Location = new System.Drawing.Point(6, 194);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(93, 13);
            this.label20.TabIndex = 18;
            this.label20.Text = "Valor Instantâneo:";
            // 
            // text_ID_SCorrente
            // 
            this.text_ID_SCorrente.BackColor = System.Drawing.Color.White;
            this.text_ID_SCorrente.Font = new System.Drawing.Font("Microsoft Sans Serif", 36F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.text_ID_SCorrente.Location = new System.Drawing.Point(10, 121);
            this.text_ID_SCorrente.Multiline = true;
            this.text_ID_SCorrente.Name = "text_ID_SCorrente";
            this.text_ID_SCorrente.ReadOnly = true;
            this.text_ID_SCorrente.Size = new System.Drawing.Size(154, 70);
            this.text_ID_SCorrente.TabIndex = 17;
            this.text_ID_SCorrente.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Location = new System.Drawing.Point(7, 16);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(95, 13);
            this.label21.TabIndex = 14;
            this.label21.Text = "ID do Transmissor:";
            // 
            // text_TX_ID_SCorrente
            // 
            this.text_TX_ID_SCorrente.BackColor = System.Drawing.Color.White;
            this.text_TX_ID_SCorrente.Font = new System.Drawing.Font("Microsoft Sans Serif", 36F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.text_TX_ID_SCorrente.Location = new System.Drawing.Point(10, 32);
            this.text_TX_ID_SCorrente.Multiline = true;
            this.text_TX_ID_SCorrente.Name = "text_TX_ID_SCorrente";
            this.text_TX_ID_SCorrente.ReadOnly = true;
            this.text_TX_ID_SCorrente.Size = new System.Drawing.Size(154, 70);
            this.text_TX_ID_SCorrente.TabIndex = 16;
            this.text_TX_ID_SCorrente.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Location = new System.Drawing.Point(6, 105);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(150, 13);
            this.label22.TabIndex = 15;
            this.label22.Text = "ID do Sensor de Temperatura:";
            // 
            // groupBox5
            // 
            this.groupBox5.Controls.Add(this.text_SCorrente_RCP_ID);
            this.groupBox5.Controls.Add(this.button_SCorrente_Enviar);
            this.groupBox5.Controls.Add(this.text_SCorrente_ID);
            this.groupBox5.Controls.Add(this.label8);
            this.groupBox5.Controls.Add(this.label17);
            this.groupBox5.Location = new System.Drawing.Point(4, 4);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Size = new System.Drawing.Size(170, 190);
            this.groupBox5.TabIndex = 0;
            this.groupBox5.TabStop = false;
            this.groupBox5.Text = "Comandos";
            // 
            // text_SCorrente_RCP_ID
            // 
            this.text_SCorrente_RCP_ID.Location = new System.Drawing.Point(10, 36);
            this.text_SCorrente_RCP_ID.Name = "text_SCorrente_RCP_ID";
            this.text_SCorrente_RCP_ID.Size = new System.Drawing.Size(154, 20);
            this.text_SCorrente_RCP_ID.TabIndex = 13;
            this.text_SCorrente_RCP_ID.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // button_SCorrente_Enviar
            // 
            this.button_SCorrente_Enviar.Location = new System.Drawing.Point(10, 104);
            this.button_SCorrente_Enviar.Name = "button_SCorrente_Enviar";
            this.button_SCorrente_Enviar.Size = new System.Drawing.Size(154, 31);
            this.button_SCorrente_Enviar.TabIndex = 12;
            this.button_SCorrente_Enviar.Text = "Enviar Comando";
            this.button_SCorrente_Enviar.UseVisualStyleBackColor = true;
            // 
            // text_SCorrente_ID
            // 
            this.text_SCorrente_ID.Location = new System.Drawing.Point(10, 78);
            this.text_SCorrente_ID.Name = "text_SCorrente_ID";
            this.text_SCorrente_ID.Size = new System.Drawing.Size(154, 20);
            this.text_SCorrente_ID.TabIndex = 11;
            this.text_SCorrente_ID.TextAlign = System.Windows.Forms.HorizontalAlignment.Center;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(7, 61);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(72, 13);
            this.label8.TabIndex = 10;
            this.label8.Text = "ID do Sensor:";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Location = new System.Drawing.Point(7, 20);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(83, 13);
            this.label17.TabIndex = 9;
            this.label17.Text = "ID do Receptor:";
            // 
            // tabPage1
            // 
            this.tabPage1.Controls.Add(this.cartesian_STemp3);
            this.tabPage1.Location = new System.Drawing.Point(4, 22);
            this.tabPage1.Name = "tabPage1";
            this.tabPage1.Size = new System.Drawing.Size(1280, 556);
            this.tabPage1.TabIndex = 5;
            this.tabPage1.Text = "Temperatura Ambiente";
            this.tabPage1.UseVisualStyleBackColor = true;
            // 
            // cartesian_STemp3
            // 
            this.cartesian_STemp3.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.cartesian_STemp3.BackColor = System.Drawing.Color.White;
            this.cartesian_STemp3.Location = new System.Drawing.Point(96, 13);
            this.cartesian_STemp3.Name = "cartesian_STemp3";
            this.cartesian_STemp3.Size = new System.Drawing.Size(1088, 530);
            this.cartesian_STemp3.TabIndex = 1;
            // 
            // ControlSystem
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.AutoSize = true;
            this.ClientSize = new System.Drawing.Size(1308, 604);
            this.Controls.Add(this.tabControl);
            this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
            this.Name = "ControlSystem";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "Sistema Supervisório";
            this.WindowState = System.Windows.Forms.FormWindowState.Maximized;
            this.Load += new System.EventHandler(this.ControlSystem_Load);
            this.groupBoxTCP.ResumeLayout(false);
            this.groupBoxTCP.PerformLayout();
            this.groupBoxSerial.ResumeLayout(false);
            this.groupBoxSerial.PerformLayout();
            this.tabControl.ResumeLayout(false);
            this.tab_configuracao.ResumeLayout(false);
            this.groupBox13.ResumeLayout(false);
            this.groupBox13.PerformLayout();
            this.tab_log.ResumeLayout(false);
            this.tab_Temperatura_2.ResumeLayout(false);
            this.groupBox4.ResumeLayout(false);
            this.groupBox4.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            this.tab_temperatura_1.ResumeLayout(false);
            this.groupBox7.ResumeLayout(false);
            this.groupBox6.ResumeLayout(false);
            this.groupBox6.PerformLayout();
            this.groupBox5.ResumeLayout(false);
            this.groupBox5.PerformLayout();
            this.tabPage1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBoxTCP;
        private System.Windows.Forms.Button button_tcp;
        private System.Windows.Forms.TextBox tcp_port;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox tcp_ip;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.GroupBox groupBoxSerial;
        private System.Windows.Forms.Button button_com;
        private System.Windows.Forms.ComboBox com_baud;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.ComboBox com_port;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TabControl tabControl;
        private System.Windows.Forms.TabPage tab_configuracao;
        private System.Windows.Forms.TabPage tab_log;
        private System.Windows.Forms.Button buttonSaveLOG;
        private System.Windows.Forms.Button buttonCleanLOG;
        private System.Windows.Forms.RichTextBox textLOG;
        private System.Windows.Forms.TabPage tab_Temperatura_2;
        private System.Windows.Forms.TabPage tab_temperatura_1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Button button_STemp_Enviar;
        private System.Windows.Forms.TextBox text_STemp_ID;
        private System.Windows.Forms.GroupBox groupBox3;
        private LiveCharts.WinForms.CartesianChart cartesian_STemp;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.TextBox text_ID_STemp;
        private System.Windows.Forms.TextBox text_TX_ID_STemp;
        private System.Windows.Forms.TextBox text_STemp_TX_ID;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.TextBox text_Temperatura_Inst;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.GroupBox groupBox7;
        private LiveCharts.WinForms.CartesianChart cartesian_STemp2;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.TextBox text_Corrente_Inst;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.TextBox text_ID_SCorrente;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.TextBox text_TX_ID_SCorrente;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.TextBox text_SCorrente_RCP_ID;
        private System.Windows.Forms.Button button_SCorrente_Enviar;
        private System.Windows.Forms.TextBox text_SCorrente_ID;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.GroupBox groupBox13;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.CheckBox check_JSON;
        private System.Windows.Forms.TabPage tabPage1;
        private LiveCharts.WinForms.CartesianChart cartesian_STemp3;
    }
}

